<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\BooksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Книги';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="books-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
//            'author_full_name',
            [
                'attribute' => 'author',
                'value' => function ($model, $key, $index, $column) {
                    return $model->author_full_name;
                },
            ],

            'name',
            [
                'attribute' => 'created_at',
                'value' => function ($data) {
                    return '<span class="nowrap">' . Yii::$app->formatter->asDatetime($data['created_at'],
                            'dd-MM-yyyy HH:mm:ss') . '</span>';
                },
                'format' => 'html',
            ],
            [
                'attribute' => 'updated_at',
                'value' => function ($data) {
                    return '<span class="nowrap">' . Yii::$app->formatter->asDatetime($data['updated_at'],
                            'dd-MM-yyyy HH:mm:ss') . '</span>';
                },
                'format' => 'html',
            ],

            ['class' => 'yii\grid\ActionColumn', 'template' => '{view}'],
        ],
    ]); ?>


</div>
