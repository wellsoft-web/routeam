<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "books".
 *
 * @property int $id
 * @property int $author
 * @property string $name
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Authors $author0
 * @property string $author_full_name
 * @property string $string_created_at
 */
class Books extends \yii\db\ActiveRecord
{
    public $_dateFormat;

    public $dataFormatDefault = 'd.m.Y h:i';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'books';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['author', 'name', 'created_at', 'updated_at'], 'required'],
            [['author', 'created_at', 'updated_at'], 'integer'],
            [['name', 'author_full_name', 'string_created_at'], 'string', 'max' => 255],
            ['string_created_at', 'date', 'format' => $this->dateFormat],
            [['author'], 'exist', 'skipOnError' => true, 'targetClass' => Authors::className(), 'targetAttribute' => ['author' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'author' => 'Автор',
            'name' => 'Название',
            'created_at' => 'Создана',
            'updated_at' => 'Изменена',

            'author_full_name' => 'Автор',
            'string_created_at' => 'Создана',
        ];
    }

    public function getString_created_at()
    {
      return $this->created_at ? date($this->dateFormat, $this->created_at) : '';
    }

    public function setString_created_at($value)
    {
      $this->created_at = $value ? strtotime($value) : null;
    }

    public function getDateFormat()
    {
      return $this->_dateFormat ?: $this->dataFormatDefault;
    }

    public function setDateFormat($value)
    {
      $this->dateFormat = $this->_dateFormat = $value ?: $this->dataFormatDefault;
    }

    public function getAuthor_full_name()
    {
        return $this->author0->name
            . ($this->author0->middle_name ?' ' . $this->author0->middle_name :'')
            . ($this->author0->last_name ?' ' . $this->author0->last_name :'');
    }

    /**
     * Gets query for [[Author0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor0()
    {
        return $this->hasOne(Authors::className(), ['id' => 'author']);
    }
}
