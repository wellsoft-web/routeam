<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "authors".
 *
 * @property int $id
 * @property string $name
 * @property string|null $middle_name
 * @property string|null $last_name
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Books[] $books
 * @property int $bookscount
 * @property string $author_full_name
 * @property string $string_created_at
 */
class Authors extends \yii\db\ActiveRecord
{
    public $_dateFormat;

    public $dataFormatDefault = 'd.m.Y h:i';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'authors';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'created_at', 'updated_at'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['name', 'middle_name', 'last_name'], 'string', 'max' => 255],
            ['string_created_at', 'date', 'format' => $this->dateFormat],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'middle_name' => 'Отчество',
            'last_name' => 'Фамилия',
            'bookscount' => 'Книг',
            'created_at' => 'Создан',
            'updated_at' => 'Изменен',

            'author_full_name' => 'Автор',
            'string_created_at' => 'Создана',
        ];
    }

/*
    public function fields()
    {
        $fields = parent::fields();
        $fields['bookscount'] = function () {
            return $this->getBooks()->count();
        };
        return $fields;
    }
*/

    public function getString_created_at()
    {
      return $this->created_at ? date($this->dateFormat, $this->created_at) : '';
    }

    public function setString_created_at($value)
    {
      $this->created_at = $value ? strtotime($value) : null;
    }

    public function getDateFormat()
    {
      return $this->_dateFormat ?: $this->dataFormatDefault;
    }

    public function setDateFormat($value)
    {
      $this->dateFormat = $this->_dateFormat = $value ?: $this->dataFormatDefault;
    }

    public function getAuthor_full_name()
    {
        return $this->name
            . ($this->middle_name ?' ' . $this->middle_name :'')
            . ($this->last_name ?' ' . $this->last_name :'');
    }

    public function getBookscount()
    {
      return $this->getBooks()->count();
    }

    /**
     * Gets query for [[Books]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBooks()
    {
        return $this->hasMany(Books::className(), ['author' => 'id']);
    }
}
