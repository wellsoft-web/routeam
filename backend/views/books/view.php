<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Books */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => '�����', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="books-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('��������', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('�������', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '�� �������, ��� ������ ������� ������?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
//            'author',
            'author_full_name',
            'name',
            [
                'attribute' => 'created_at',
                'format' => ['datetime', 'php:d.m.Y h:i:s'],
            ],
            [
                'attribute' => 'updated_at',
                'format' => ['datetime', 'php:d.m.Y h:i:s'],
            ],
//            'created_at',
//            'updated_at',
        ],
    ]) ?>

</div>
