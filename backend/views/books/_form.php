<?php

//use \kartik\datetime\DateTimePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use zhuravljov\yii\widgets\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\Books */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="books-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'author')->textInput() ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'string_created_at')->widget(DateTimePicker::className(), [
        'clientOptions' => [
            'formatType' => 'php',
            'format' => $model->dateFormat,
            'language' => 'ru',
            'autoclose' => true,
            'todayHighlight' => true,
        ],
        'clientEvents' => [],
    ]) ?>

    <?php
/*
    echo $form->field($model, 'created_at')->widget(DateTimePicker::className(),[
        'name' => 'created_at',
        'model' => $model,
        'attribute' => 'created_at',
        'type' => DateTimePicker::TYPE_COMPONENT_PREPEND, //DateTimePicker::TYPE_INPUT,
        'options' => ['placeholder' => 'Ввод даты/времени...'],
        'convertFormat' => true,
//        'value' => $model->created_at,
        'value' => date("d.m.Y h:i",(integer) $model->created_at),
        'language' => 'ru',
        'layout' => '{picker}{input}{remove}',
        'pickerButton' => ['icon' => 'time'],
        'pluginOptions' => [
            'format' => 'dd.MM.yyyy hh:i',
            'autoclose'=> true,
            'weekStart'=> 1, //неделя начинается с понедельника
            'startDate' => '01.05.2015 00:00', //самая ранняя возможная дата
            'todayBtn'=> true, //снизу кнопка "сегодня"
            'todayHighlight' => true,
        ]
    ]);
*/
    ?>

    <?php /*= $form->field($model, 'updated_at')->textInput() */?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
