<?php

use yii\db\Migration;

class m200624_000257_add_bookslibrary_tables extends Migration
{
  public function up()
  {
    $tableOptions = null;
    if ($this->db->driverName === 'mysql') {
      // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
      $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    }

    $this->createTable('{{%authors}}', [
      'id' => $this->primaryKey(),
      'name' => $this->string()->notNull(),
      'middle_name' => $this->string(),
      'last_name' => $this->string(),

      'created_at' => $this->integer()->notNull(),
      'updated_at' => $this->integer()->notNull(),
    ], $tableOptions);

    $this->createTable('{{%books}}', [
      'id' => $this->primaryKey(),
      'author' => $this->integer()->notNull(),
      'name' => $this->string()->notNull(),

      'created_at' => $this->integer()->notNull(),
      'updated_at' => $this->integer()->notNull(), ## ��� ���������� � ����� Yii �������� "�������" ���������� INT(11), ��� �������������� ������� �����������.
    
      'INDEX ([[author]])',
      'FOREIGN KEY ([[author]]) REFERENCES [[authors]] ([[id]]) ON DELETE CASCADE',
    ], $tableOptions);
  }

  public function down()
  {
    $this->dropTable('{{%authors}}');
    $this->dropTable('{{%books}}');
  }
}
